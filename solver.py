import networkx as nx
import operator


def solve(client):

    def solver_1():

        client.end()    # terminate existing rescue
        client.start()  # starts new rescue

        G = client.G

        # Dijkstra graph
        print("\nDijkstra weights = {}".format(list(nx.single_source_dijkstra(G, client.home))[0]))
        print("\nDijkstra paths = {}".format(list(nx.single_source_dijkstra(G, client.home))[1]))

        # Displaying some of the sets we're working with
        all_students = list(range(1, client.students+1))
        total_students = len(all_students)
        non_home_vertices = list(range(1, client.home)) + list(range(client.home+1, client.v+1))
        print("\nStudents = {}".format(all_students))
        print("\nVertices-home = {}".format(non_home_vertices))
        print("\nHome vertex = {}\n".format(client.home))

        # This returns a hash of all yeses that have been reported for that vertex
        true_vertices = dict()

        # This hash maps every student to another hash which uses vertices as its key
        # and the student's response to that vertex as the dictionary's value
        student_claims = dict()
        for i in all_students:
            student_claims[i] = dict()

        temp_list = non_home_vertices
        for vertex in temp_list:
            count = client.scout(vertex, all_students)
            for key, value in count.items():
                student_claims[key][vertex] = value
            num_true = 0
            for value in count.values():
                if value is True:
                    num_true += 1
            true_vertices[vertex] = num_true

        # STUDENT UPDATE METHOD #

        if total_students <= 10:
            positive = 1.01
            negative = 1.00
        if total_students <= 25:
            positive = 1.007     # 1.02
            negative = 1.00
        if total_students > 25:
            positive = 1.008    # 1.014
            negative = 1.00

        def multiplicative_update(answer):      # currently inverted weights

            for s in student_updated_weights:
                if student_claims[s][u_path[i]] ^ answer is False:
                    student_updated_weights[s] *= positive
                else:
                    student_updated_weights[s] *= negative

        def store_initial_bot_position():
            counter = 0
            while counter < len(sorted_yes_per_vertex):
                if u_path[i] == sorted_yes_per_vertex[counter][0]:
                    furthest_vertex[u_path[i]] = counter
                    break
                counter += 1

        # This hash maps updated weight to each student -- weight updates after every iteration
        student_updated_weights = dict()
        for i in all_students:
            student_updated_weights[i] = 1

        # This hash maps each vertex to the set of all students that reported "yes" to that vertex
        students_that_say_vertex_true = dict()
        for v in non_home_vertices:
            students_that_say_vertex_true[v] = list()
        for key, val in student_claims.items():
            for k, v in val.items():
                if v is True:
                    students_that_say_vertex_true[k].append(key)

        print("\nStudent yeses for vertices = {}\n".format(students_that_say_vertex_true))
        print("\nStudent weights = {}\n".format(student_updated_weights))

        # Initializes vertex weights to the initial number of reported yeses
        total_vertex_weights = dict()
        for v in non_home_vertices:
            total_vertex_weights[v] = len(students_that_say_vertex_true[v])
        print("\nVertex totals = {}\n".format(sorted(total_vertex_weights.items(), key=operator.itemgetter(1), reverse=True)))

        # Making a copy of vertices and the number of yeses
        sorted_yes_per_vertex = sorted(total_vertex_weights.items(), key=operator.itemgetter(1), reverse=True)
        dijkstra_graph = list(nx.single_source_dijkstra(G, client.home))[1]
        dijkstra_graph_weights = list(nx.single_source_dijkstra(G, client.home))[0]

        # Keeps track of the furthest vertex reached
        furthest_vertex = dict()

        # current home count
        current_home_count = 0

        # Single element set that will be updated with a new vertex after every iteration
        test_vertices = list()

        # This variable keeps track of vertex iterations.
        vertex_iteration = 1

        while client.bot_count[client.home] < client.l:

            # Finding maximum valued vertex
            maximum = 0
            k = None
            for key, value in total_vertex_weights.items():
                if value > maximum:
                    maximum = value
                    k = key
            test_vertices.append(k)

            for key in test_vertices:
                del total_vertex_weights[key]

            for u in test_vertices:
                u_path = dijkstra_graph[u]
                i = len(u_path)-1
                while i >= 1:
                    client.remote(u_path[i], u_path[i-1])

                    # if vertex remoted to has 0 bots
                    if client.bot_count[u_path[i-1]] < 1:
                        multiplicative_update(False)
                        break

                    # if vertex remoted to has 1 or more bots and we're not at home vertex
                    if (client.bot_count[u_path[i-1]] >= 1) and (u_path[i-1] != client.home):
                        if i == len(u_path)-1:
                            furthest_vertex[u_path[i]] = 0
                            store_initial_bot_position()
                        pass

                    # if vertex remoted to has 1 or more bots and we are at home vertex
                    if (client.bot_count[u_path[i-1]] >= 1) and (u_path[i-1] == client.home):
                        if current_home_count < client.bot_count[client.home]:
                            if i == len(u_path) - 1:
                                furthest_vertex[u_path[i]] = 0
                                store_initial_bot_position()
                            current_home_count += 1
                            multiplicative_update(True)
                        else:
                            multiplicative_update(False)
                    i -= 1

                print("Vertex iteration number {}".format(vertex_iteration))
                vertex_iteration += 1

                if client.bot_count[client.home] == client.l:
                    print("\nFurthest bot position = {}".format(max(furthest_vertex.values())+1))
                    print("\nInitial bot location hash = {}".format(furthest_vertex))
                    break
            test_vertices.remove(test_vertices[0])

            for p in total_vertex_weights:
                total_vertex_weights[p] = 0

            for key, value in total_vertex_weights.items():
                for a in students_that_say_vertex_true[key]:
                    total_vertex_weights[key] += student_updated_weights[a]

        print("\nNumber of students = {}".format(total_students))
        print("\nTotal vertex weights = {}".format(total_vertex_weights))
        print("\nStudent weights = {}".format(student_updated_weights))
        print("\nHeaviest weight = {}\n".format(max(dijkstra_graph_weights.values())))

        client.end()

    solver_1()      # newer version


### IDEAS ###

# instead of remoting them home as soon as they're found, just find one path that brings them all home
# update the weights based on the number of students
    # --> the fewer students, the greater the weight.... the more students, the less the weight (or the other
    # way around... whatever works best)
    # and try reversing weights...
# look out for repeating remotes
# MAYBE CONSIDER STUDENTS THAT CLAIM TRUE BY THEMSELVES
# we should focus on getting the top 85% higher even if it reduces the the bottom 15% since we won't be counting those
# different solvers are reacting differently to different weights. Sample for different weights
